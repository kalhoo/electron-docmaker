// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
import * as GoldenLayout from 'golden-layout';
import * as CodeMirror from 'codemirror';

var config = {
    content: [{
        type: 'row',
        content:[{
            type: 'component',
            componentName: 'componentFolders',
            componentState: { label: 'A' },
            title:'Dossiers',
            width: 10
        },{
            type: 'column',
            content:[{
                type: 'component',
                componentName: 'componentFile',
                componentState: { label: 'B' },
                title:'Fichier',
                height: 80
            },{
                type: 'component',
                componentName: 'componentConsole',
                componentState: { label: 'C' },
                title:'Console',
            }]
        },{
            type: 'component',
            componentName: 'componentRender',
            componentState: { label: 'A' },
            title:'Rendu'
        }]
    }]
};

var globalLayout = new GoldenLayout( config );

globalLayout.registerComponent( 'componentFolders', function( container: any, componentState: any ){
    container.getElement().html( '<h2>' + componentState.label + '</h2>' );
});
globalLayout.registerComponent( 'componentFile', function( container: any, componentState: any ){
    container.getElement().html( '<textarea id="ta-codemirror" style="width:100%;min-height:100%;"></textarea>' );
});
globalLayout.registerComponent( 'componentRender', function( container: any, componentState: any ){
    container.getElement().html( '<h2>' + componentState.label + '</h2>' );
});
globalLayout.registerComponent( 'componentConsole', function( container: any, componentState: any ){
    container.getElement().html( '<img src="https://fictionmachine.files.wordpress.com/2015/11/bttf2_05.jpg" width="100" height="100" />');
});

globalLayout.init();


let myTextArea : any;
myTextArea = document.getElementById('ta-codemirror1');
/*
CodeMirror.fromTextArea(myTextArea, {
    lineNumbers: true,
    lineWrapping: true,
    value: "function myScript(){return 100;}\n",
    mode: "markdown",
  });*/
var myCodeMirror = CodeMirror(document.body, {
  value: "function myScript(){return 100;}\n",
  mode:  "javascript",
  lineWrapping: true,
});
